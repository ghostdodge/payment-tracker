package com.paymenttracker;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.paymenttracker.service.ConfigurationService;
import com.paymenttracker.service.DataService;
import com.paymenttracker.service.DisplayService;

@SpringBootApplication
public class PaymentTrackerApplication implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	private DataService dataService;
	
	@Autowired
	private ConfigurationService configurationService;
	
	@Autowired
	private DisplayService displayService;
	
	public static void main(String[] args) {
		SpringApplication.run(PaymentTrackerApplication.class, args);
	}

	public void onApplicationEvent(ContextRefreshedEvent arg) {
		configurationService.loadConfiguration();
		dataService.loadData();
		
		displayService.startDisplaying();
	}
}
