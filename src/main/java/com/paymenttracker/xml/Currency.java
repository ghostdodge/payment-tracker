package com.paymenttracker.xml;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

@Root(name = "currency")
public class Currency {
	
	@Attribute(name = "name")
	public String name;
	
	@Attribute(name = "usd_rate")
	public Double usdRate;
	
}
