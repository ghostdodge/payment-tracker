package com.paymenttracker.xml;

import java.util.LinkedList;
import java.util.List;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

@Root(name = "configuration")
public class Configuration {
	
	@Attribute(name = "load_rates")
	public boolean loadRates;
	
	@ElementList(name = "currency", inline = true)
	public List<Currency> currencies = new LinkedList<Currency>();

}
