package com.paymenttracker.service;

import java.io.File;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DataService extends Service {
	private static final String LINE_REGEX = "^([A-Z]{3}) (-?\\d+(\\.\\d+)?)$";
	
	@Value("${dataFile:#{null}}")
	private String filePath;
	
	private Map<String, Double> data = new HashMap<String, Double>();
	
	public void loadData() {
		if (filePath != null) {
			File file = new File(filePath);
			
			if (file.isFile() && file.exists()) {
				for (String line: loadFile(filePath)) {
					addLine(line);
				}
			}
		}
	}
	
	public void storeData() {
		if (filePath != null) {
			List<String> lines = new LinkedList<String>();
			
			for (String currency : data.keySet()) {
				Double amount = data.get(currency);
				
				lines.add(makeLine(currency, amount));
			}
			
			storeFile(filePath, lines);
		}
	}
	
	public synchronized List<String> getLines() {
		List<String> lines = new LinkedList<String>();
		
		for (String currency : data.keySet()) {
			Double amount = data.get(currency);
			
			lines.add(makeLine(currency, amount));
		}
		
		return lines;
	}
	
	public synchronized void addLine(String line) {
		Pattern pattern = Pattern.compile(LINE_REGEX);
		Matcher matcher = pattern.matcher(line);
		
		if (matcher.matches()) {
			String currency = matcher.group(1);
			String amountString = matcher.group(2);
			Double amount = new Double(amountString);
			
			if (data.containsKey(currency)) {
				amount = data.get(currency) + amount;
				
				putRemoveOnZero(currency, amount);
				
			} else {
				putRemoveOnZero(currency, amount);
			}
			
		} else {
			System.out.println("Invalid entry (skipped): " + line);
		}
	}
	
	private void putRemoveOnZero(String currency, Double amount) {
		if (amount != 0) {
			data.put(currency, amount);
			
		} else {
			data.remove(currency);
		}
	}
	
	private String makeLine(String currency, Double amount) {
		return currency + " " + String.format(Locale.ENGLISH, "%.2f", amount);
	}

}
