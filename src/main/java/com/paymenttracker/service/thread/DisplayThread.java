package com.paymenttracker.service.thread;

import java.util.List;
import java.util.Locale;
import java.util.TimerTask;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.paymenttracker.service.ConfigurationService;
import com.paymenttracker.service.DataService;
import com.paymenttracker.xml.Configuration;

@Component
public class DisplayThread extends TimerTask {
	
	@Autowired
	private DataService dataService;
	
	@Autowired
	private ConfigurationService configurationService;

	@Override
	public void run() {
		List<String> lines = dataService.getLines();
		
		for (String line : lines) {
			String[] atoms = line.split(" ");
			String currency = atoms[0];
			Double amount = new Double(atoms[1]);
			Double usdRate = getUsdRate(currency);
			
			if (usdRate != null) {
				line += " (USD " + String.format(Locale.ENGLISH, "%.2f", (amount / usdRate)) + ")";
			}
			
			System.out.println(line);
		}
	}
	
	private Double getUsdRate(String currency) {
		Configuration configuration = configurationService.getConfiguration();
		
		if (configuration != null && configuration.loadRates) {
			return configurationService.findRate(currency);
		}
		
		return null;
	}

}
