package com.paymenttracker.service.thread;

import java.util.Scanner;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.paymenttracker.service.ConfigurationService;
import com.paymenttracker.service.DataService;

@Component
public class InputThread implements Runnable {
	
	@Autowired
	private DataService dataService;
	
	@Autowired
	private ConfigurationService configurationService;

	public void run() {
		Scanner scanner = new Scanner(System.in);
		
		while (scanner.hasNextLine()) {
			String input = scanner.nextLine();
			
			if ("quit".equals(input)) {
				dataService.storeData();
				configurationService.storeConfiguration();
				
				System.exit(0);
			}
			
			dataService.addLine(input);
		}
		
		scanner.close();
	}

}
