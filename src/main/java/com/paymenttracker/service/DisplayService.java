package com.paymenttracker.service;

import java.util.Timer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.paymenttracker.service.thread.DisplayThread;
import com.paymenttracker.service.thread.InputThread;

@Component
public class DisplayService {
	private static final int DISPLAY_DURATION = 60000;
	
	@Autowired
	private DisplayThread displayThread;
	
	@Autowired
	private InputThread inputThread;
	
	public void startDisplaying() {
		new Timer().schedule(displayThread, 0, DISPLAY_DURATION);
		new Thread(inputThread).start();
	}
	
}
