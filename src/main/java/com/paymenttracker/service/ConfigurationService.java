package com.paymenttracker.service;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.paymenttracker.xml.Configuration;
import com.paymenttracker.xml.Currency;

@Component
public class ConfigurationService extends Service {
	
	@Value("${configFile:#{null}}")
	private String filePath;
	
	private Configuration configuration;
	
	public void loadConfiguration() {
		if (filePath != null) {
			File configFile = new File(filePath);
			
			if (configFile.exists() && configFile.isFile()) {
				configuration = (Configuration) loadXmlFile(Configuration.class, filePath);
				
				return;
			}
		}
		
		configuration = (Configuration) loadXmlResource(Configuration.class, "com/paymenttracker/config.xml");
	}
	
	public void storeConfiguration() {
		if (filePath != null) {
			storeXmlFile(configuration, filePath);
		}
	}
	
	public Double findRate(String currencyName) {
		for (Currency currency : configuration.currencies) {
			if (currency.name.equals(currencyName)) {
				return currency.usdRate;
			}
		}
		
		return null;
	}
	
	public Configuration getConfiguration() {
		return configuration;
	}

}
