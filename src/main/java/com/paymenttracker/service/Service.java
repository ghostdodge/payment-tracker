package com.paymenttracker.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.simpleframework.xml.Serializer;
import org.simpleframework.xml.core.Persister;

public abstract class Service {
	
	private Serializer serializer = new Persister();
	
	@SuppressWarnings("unchecked")
	protected List<String> loadFile(String path) {
		try {
			return FileUtils.readLines(new File(path));
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected Object loadXmlFile(Class<?> clazz, String path) {
		try {
			return serializer.read(clazz, new File(path));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected Object loadXmlResource(Class<?> clazz, String path) {
		try {
			return serializer.read(clazz, java.lang.ClassLoader.getSystemResourceAsStream(path));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
	protected void storeFile(String path, List<String> lines) {
		try {
			FileUtils.writeLines(new File(path), lines);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	protected void storeXmlFile(Object object, String path) {
		try {
			serializer.write(object, new File(path));
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
