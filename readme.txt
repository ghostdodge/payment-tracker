This document describes how to setup and use Payment Tracker application. Application
show tracked payments and user is able to add payments one by one, or by using import
file. Example of application output:

CZK 440.0 (USD 19.1)
EUR 23.5
USD 12.8

Prerequisites:

* Java installed in order to proceed
* Maven installed for building the application

---------------
1. INSTALLATION
---------------

1.1. Clone from git:
--------------------
From destination directory run git clone command from cli:

git clone git@bitbucket.org:ghostdodge/payment-tracker.git

Command will create application directory called "payment-tracker".

1.2. Build:
-----------
Change to application directory and run maven package command:

cd payment-tracker
mvn package

This creates application archive in target directory.

--------
2. USAGE
--------

2.1 Running an application:
---------------------------
From application directory run a command:

java -jar target/payment-tracker-0.0.1-SNAPSHOT.jar

2.2 Track new payment:
----------------------
In running application type payment in format:

<CURRENCY> <AMOUNT>

And confirm with enter key. Currency has to be 3 leters uppercase string. Amount is
dot separated, decimal number. Example:

EUR 12.5

2.3 Quitting an application:
----------------------------
Just type "quit" command and confirm with enter key when application is runnig.

2.4 Configure data file:
------------------------
Data file is used for both loading data on startup and storing data when application is
being stopped.

For setting data file use --dataFile argument when you run the application. Example:

java -jar target/payment-tracker-0.0.1-SNAPSHOT.jar --dataFile=data.txt

If given data file not exist, will be created.

2.5 Setting currency USD rates:
-------------------------------
If you want application be able to show every payment amount in USD, you need to set
currency rates in configuration xml file. Example of this file:

<configuration load_rates="true">
   <currency name="CZK" usd_rate="23.8"/>
</configuration>

When "load_rates" is set to true. Application will show USD amount in bracket. Example
output:

EUR 100.00 (USD 113.64)

You can provide this configuration by adding --configFile property to your application
run command. Example:

java -jar target/payment-tracker-0.0.1-SNAPSHOT.jar --configFile=config.xml

When given configuration file does not exist, default one will be created instead. This
can be used for creating new configuration file, but notice that in default config.xml
file "load_rates" attribute is set to false. When you want to show payments USD amount
set this attribute value to true.

